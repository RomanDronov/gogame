import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image } from "react-native";
import { Title, Button } from 'react-native-paper';

export default function LoginScreen() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Title style={styles.title}>Login</Title>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Email</Text>
        <Image source={require('../assets/icons/email.png')} style={styles.icon} />
        <TextInput style={styles.textInput}
          placeholder="Enter your e-mail..."
          value={email}
          onChangeText={email => setEmail(email)}
        />
      </View>

      <View style={styles.inputContainer}>
        <Text style={styles.label}>Password</Text>
        <Image source={require('../assets/icons/eye.png')} style={styles.icon} />
        <TextInput style={styles.textInput}
          placeholder="Enter your password..."
          secureTextEntry={true}
          value={password}
          onChangeText={password => setPassword(password)}
        />
      </View>

      <Button
        style={(email && password) ? styles.loginButton : [styles.loginButton, styles.disabledButton]}
        mode="contained"
        onPress={() => console.log('Pressed')}>
        <Text style={styles.buttonText}>Login</Text>
      </Button>

      <TouchableOpacity>
        <Text style={styles.registerText}>Don’t have an account? Register now!</Text>
      </TouchableOpacity>

      <View style={styles.horizontalContainer}>
        <Image source={require('../assets/icons/apple.png')} style={styles.socialsIcon} />
        <Image source={require('../assets/icons/google.png')} style={styles.socialsIcon} />
        <Image source={require('../assets/icons/fb.png')} style={styles.socialsIcon} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    filter: 'drop-shadow(0px 8px 16px rgba(17, 17, 17, 0.04))',
    borderRadius: 32,
  },
  horizontalContainer: {
    width: '70%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    alignSelf: 'flex-start',
    marginLeft: 50,
    marginBottom: 16,
    fontWeight: "bold",
    fontSize: '32px',
    color: "#14142B",
  },
  inputContainer: {
    position: 'relative',
    marginHorizontal: 50,
    marginVertical: 12,
    borderRadius: 16
  },
  label: {
    position: 'absolute',
    left: 60,
    fontWeight: 500,
    fontSize: 14,
    lineHeight: 24,
    letterSpacing: '0.75px',
    color: '#6E7191'
  },
  icon: {
    position: 'absolute',
    top: '50%',
    left: 20,
    transform: 'translateY(-50%)',
    width: 25,
    height: 20,
  },
  textInput: {
    paddingTop: 26,
    paddingBottom: 5,
    paddingLeft: 60,
    paddingRight: 20,
    backgroundColor: "#EFF0F7",
    borderRadius: 16,
    fontSize: '16px',
    color: '#828282',
    overflow: 'hidden',
    outlineColor: '#5F2EEA'
  },
  loginButton: {
    marginTop: 16,
    marginBottom: 13,
    marginHorizontal: 50,
    paddingVertical: 5,
    paddingHorizontal: 90,
    backgroundColor: "#5F2EEA",
    borderRadius: 16,
  },
  disabledButton: {
    backgroundColor: "#B0AFE5",
  },
  buttonText: {
    fontWeight: 600,
    fontSize: '16px',
    lineHeight: '28px',
    textTransform: 'none',
  },
  registerText: {
    marginHorizontal: 70,
    marginBottom: 28,
    fontWeight: 600,
    fontSize: '16px',
    letterSpacing: '0.75px',
    textDecorationLine: 'underline',
    color: '#396FED',
    textAlign: 'center'
  },
  socialsIcon: {
    width: 52,
    height: 52
  }
});
