import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet } from "react-native";
import { configureFonts, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';

export default function App() {
  return (
    <>
      <PaperProvider>
        <LoginScreen/>
      </PaperProvider>
    </>
  );
}
